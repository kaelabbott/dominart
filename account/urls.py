from django.conf.urls import url

from account.views import *

app_name = 'account'



urlpatterns = [
    # ex: /polls/
    url(r'^login/', login_v, name='login'),
    # ex: /polls/5/
    url(r'^register/',SignUp.as_view(template_name="signup.html") , name='register'),
]
