# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from workshops.views import *

from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()
urlpatterns = [

    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^home/', home,name ='home'),
    url(r'^account/', include( 'account.urls')),
    url(r'^workshops/', workshops,name = 'workshops'),
     url(r'^product/', product,name = 'product'),
]

urlpatterns += i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^', include('cms.urls')),
     

)

# This is only needed when using runserver.
if settings.DEBUG:
    import debug_toolbar


    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
