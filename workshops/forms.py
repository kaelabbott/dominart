from django import forms
from workshops.models import PROFESSION,Worker


class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput(render_value=False))
    
class UserRegisterForm(forms.Form): 
    name = forms.CharField(widget=forms.TextInput())
    last_name = forms.CharField(widget=forms.TextInput())
    email = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput(), min_length= 6)
    
    
    

class WorkerRegistrationForm(forms.Form):
    name = forms.CharField(label="Name",widget=forms.TextInput(),max_length=60)

    profession = forms.ChoiceField(
        required=False,
        widget=forms.Select(),
        choices=PROFESSION,

    )
    
    email = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput(), min_length=5)
    confirm_password = forms.CharField(widget=forms.PasswordInput(), min_length=5)


    class Meta:
        model = Worker
        exclude = ['user','workshop','manager']

    

    