# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-15 09:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workshops', '0008_auto_20160701_0702'),
    ]

    operations = [
        migrations.AddField(
            model_name='workshop',
            name='image',
            field=models.ImageField(default=1, upload_to=b''),
            preserve_default=False,
        ),
    ]
