# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workshops', '0011_auto_20160815_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workshop',
            name='image',
            field=models.ImageField(upload_to=''),
        ),
    ]
