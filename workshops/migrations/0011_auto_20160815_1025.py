# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-15 10:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workshops', '0010_auto_20160815_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.TextField(max_length=300),
        ),
        migrations.AlterField(
            model_name='workshop',
            name='description',
            field=models.TextField(max_length=300),
        ),
    ]
