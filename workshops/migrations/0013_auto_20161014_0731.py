# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('workshops', '0012_auto_20161003_0716'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customer',
            old_name='tel',
            new_name='telephone',
        ),
        migrations.RenameField(
            model_name='worker',
            old_name='telefono',
            new_name='telephone',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='e_mail',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='name',
        ),
        migrations.RemoveField(
            model_name='worker',
            name='e_mail',
        ),
        migrations.RemoveField(
            model_name='worker',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='worker',
            name='name',
        ),
        migrations.AddField(
            model_name='customer',
            name='user',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='worker',
            name='user',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='worker',
            name='workshop',
            field=models.ForeignKey(to='workshops.Workshop', blank=True, related_name='workshop_worker', null=True),
        ),
    ]
