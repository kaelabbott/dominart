# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

    

PROFESSION = (
    ('0' , 'Herrero'),
    ('1' , 'Encargado'),
    ('2', 'Ebanista') ,
    )
    

MATERIALS = (
    ('0' , 'Hierro'),
    ('1' , 'Madera'),
    ('2', 'Céramica') ,
    )


# Create your models here.



class Address(models.Model):
    province = models.CharField("Provincia", max_length=60)
    zone = models.CharField("Zona", max_length=60)
    street = models.CharField("Calle", max_length=60)
    location = models.CharField("Geolocalizacion", max_length=60)
    
    def __unicode__(self):
        return "%s %s %s %s" %(self.province, self.zone, self.street, self.location)



class Customer(models.Model):
    user=models.ForeignKey(User)
    telephone = models.CharField("Telefono", max_length=10)
    cellphone = models.CharField("Celular", max_length=60)
    Address = models.OneToOneField(Address)
    
    def __unicode__(self):
        return "%s %s %s %s " %(self.name, self.last_name, self.e_mail, self.cellphone)
    
    
        
        



    
class Company(models.Model):
    name = models.CharField("name", max_length=60)
    e_mail = models.EmailField("E-mail", max_length=60)
    Address = models.OneToOneField(Address)    
    
    
    def __unicode__(self):
        return "%s %s" %(self.name, self.e_mail)


    
class Workshop(models.Model):
    name = models.CharField(max_length=60)
    materials = models.CharField(max_length=60, choices=MATERIALS)
    image = models.ImageField()
    description = models.TextField(max_length=300)
    
    def name_materials(self):
        return "%s " % ( MATERIALS[int(self.materials)][1])

    def __unicode__(self):
        return "%s " % (self.name)

    def __str__(self):
        return "%s " % (self.name)



class Worker(models.Model):
    user=models.ForeignKey(User)
    telephone = models.CharField("Telefono", max_length=10)
    cellphone = models.CharField("Celular", max_length=60)
    address = models.OneToOneField(Address)
    workshop = models.ForeignKey(Workshop, related_name='workshop_worker',blank=True,null=True)
    manager = models.ForeignKey(Workshop,null = True , blank = True, related_name='workshop_manager')
    profession = models.CharField(max_length=60, choices=PROFESSION)
    
    def __unicode__(self):
        return "%s %s %s %s" % (self.name, self.last_name, self.e_mail, self.cellphone)




class Product(models.Model):
    name = models.CharField("Nombre" , max_length=60)
    image = models.ImageField("Imagen", upload_to = 'product/' )
    cost = models.IntegerField("Costo")
    cuantity = models.IntegerField("Cantidad")
    price = models.IntegerField("Precio")
    workshop = models.ForeignKey(Workshop)
    description = models.TextField(max_length=300)

    def __unicode__(self):
        return "%s %s %s %s %s %s" %(self.name,  self.image, self.cuantity, self.price, self.workshop, self.description)
        
