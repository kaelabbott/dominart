from django.contrib import admin

# Register your models here.

from workshops.models import * 

class WorkshopAdmin(admin.ModelAdmin):
    list_display = ('name','description')
    
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','description')

admin.site.register(Workshop, WorkshopAdmin)
admin.site.register(Worker)
admin.site.register(Product, ProductAdmin)
admin.site.register(Customer)
admin.site.register(Address)
admin.site.register(Company)
