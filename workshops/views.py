from django.http import Http404
from django.shortcuts import render
from workshops.models import *
from django.http import HttpResponseRedirect, HttpResponse
from workshops.forms import *
from django.contrib.auth import *
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login,authenticate
from django.views.generic import TemplateView

''' The home view of the project'''
def home(request):
  try:
    """to do : we must do this in a efficiently way """
    w = Workshop.objects.all()
    c = Customer.objects.all()
    p = Product.objects.all()
    s = set()
    count = 0 
    for i in w:
      if count <= 5:
        count += 1
        s.add(i)
      else:
        break
        
      
      print (s) 
    
    
  except Workshop.DoesNotExist:
    raise Http404("Poll does not exist")
  know_us = 'Conócenos'  
  home_title = 'Inicio'  
  about_us = 'Sobre nosotros'  
  login_title = u'Iniciar sesión' 
  register_tittle = 'Registrate'
  viewdetails_title = 'Ver detalles'
  products_title = 'Productos'
  workshops_title = 'Talleres'
  descripcion_title = u'Descripción'
  follow_us = 'Siguenos'
  art_title = 'Arte'
  join_us_title = '¡Unete!'
  blog_title = 'Blog'
  subsection_title = '¡Dominicreart!'
  subsection_description = '¡El unico sitio de piezas de arte por talleres de Republica Dominicana!'
  return render(request, 'index.html', locals())
  


#list_workshopsview
def workshops(request):

    
  w = Workshop.objects.all()
  know_us = 'Conócenos'  
  home_title = 'Inicio'  
  about_us = 'Sobre nosotros'  
  login_title = u'Iniciar sesión' 
  register_tittle = 'Registrate'
  viewdetails_title = 'Ver detalles'
  products_title = 'Productos'
  workshops_title = 'Talleres'
  descripcion_title = u'Descripción'
  follow_us = 'Siguenos'
  art_title = 'Arte'
  join_us_title = '¡Unete!'
  blog_title = 'Blog'
  subsection_title = '¡Dominicreart!'
  subsection_description = '¡El unico sitio de piezas de arte por talleres de Republica Dominicana!'
  return render(request, 'home/list_workshops.html', locals())
  
def product(request):

    
  p = Product.objects.all()
  know_us = 'Conócenos'  
  home_title = 'Inicio'  
  about_us = 'Sobre nosotros'  
  login_title = u'Iniciar sesión' 
  register_tittle = 'Registrate'
  viewdetails_title = 'Ver detalles'
  products_title = 'Productos'
  workshops_title = 'Talleres'
  descripcion_title = u'Descripción'
  follow_us = 'Siguenos'
  art_title = 'Arte'
  join_us_title = '¡Unete!'
  blog_title = 'Blog'
  subsection_title = '¡Dominicreart!'
  subsection_description = '¡El unico sitio de piezas de arte por talleres de Republica Dominicana!'
  return render(request, 'home/product.html', locals())
      
def login_v(request):
  msg=''
  form=LoginForm()
  print (request)
  #import ipdb
  #ipdb.set_trace()
  print (request.user)
  if request.user.is_authenticated():
    #return render_to_response('/dashboard/dashboard.html',dict(), context_instance=RequestContext(request))
    return HttpResponseRedirect('/dashboard/')


  else:
    if request.method=="POST":
      form=LoginForm(request.POST)
      ctx={'form':'','msg':''}
      #import ipdb
      #ipdb.set_trace()
      if form.is_valid():
        userid=form.cleaned_data['email']
        password=form.cleaned_data['password']
        auth_user=authenticate(username=userid,password=password)
        print (auth_user)

        if auth_user is not None and auth_user.is_active:
          login(request,auth_user)

          return HttpResponseRedirect('/dashboard/')
          #return render_to_response('dashboard/dashboard.html',dict(), context_instance=RequestContext(request))


        else:
           msg='Credenciales incorrectas.'
        form=LoginForm()
        auth_user=authenticate(username=userid,password=password)
        if auth_user is not None:
          login(request,auth_user)
          return HttpResponseRedirect('/dashboard/')
          #return render_to_response('dashboard/dashboard.html',ctx, context_instance=RequestContext(request))

        else:
          msg='Credenciales incorrectas.'

    ctx={'form':form,'msg':msg}
    return render(request, 'login.html', locals())
  
    #return render_to_response('login.html',ctx, context_instance=RequestContext(request))


''' The Signup  view'''

class SignUp(TemplateView):
  template_name = 'signup.html'
  def post(self,request, *args,**kwargs):
      msg=''
      form = UserRegisterForm(request.POST)
      if form.is_valid():
        email=form.cleaned_data['email']
        password=form.cleaned_data['password']
      if (password!=form.cleaned_data['confirm_password']):
          msg='Passwords dot not match!'
          ctx={'form':form,'msg':msg}
          return render_to_response(self.template_name,locals(), context_instance=RequestContext(request))
      new_user=''



      # We create the organization
      organization_name=form.cleaned_data['organization_name']
      industry_sector=form.cleaned_data['industry_sector']
      industry_group=form.cleaned_data['industry_group']

      new_org=''

      try:
          new_org=Organization.objects.create(name=organization_name, industry_sector=int(industry_sector),industry_group=int(industry_group))
      except:
          msg='err on organization information '
          ctx={'form':form,'msg':msg}
          return render_to_response(self.template_name,locals(), context_instance=RequestContext(request))


      try:
          #some get_or_create mechansm
          new_usr=User.objects.create_user(username=email, email=email, password=password)
          new_usp=UserProfile(user=new_usr,organization=new_org)
      except:
          msg='account with email ' + email +' alreay created!'
          ctx={'form':form,'msg':msg}
      return render_to_response(self.template_name,locals(), context_instance=RequestContext(request))

      new_org.save()
      new_usp.save()
      new_usr.save()

      new_user = authenticate(username=email,password=password)
      if new_user is not None :
          login(request,new_user)
          return HttpResponseRedirect('/dashboard/')

        #return render_to_response('dashboard/dashboard.html',locals(), context_instance=RequestContext(request))

      else:
          msg='Wrong info'
          form = UserRegisterForm()
          ctx={'form':form,'msg':msg}
          return render_to_response(self.template_name,locals(), context_instance=RequestContext(request))

  def get(self, request, *args, **kwargs):
      msg=''
      if request.user.is_authenticated():
          return redirect(Dashboard)
          form = UserRegisterForm()
      ctx={'form':form,'msg':msg}
      return render_to_response(self.template_name,locals(), context_instance=RequestContext(request))
